//
//  ViewController.swift
//  P004_SimpleTableCell
//
//  Created by yao_yu on 14-9-22.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var datas = [(String,String)]()

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
//        for i in 1...100{
//            datas.append("自定义单元格, 第\(i)项")
//        }
        datas = [("扣肉 炖肉","10分钟"),
            ("蒸肉 烤肉","10分钟"),
            ("鱼香肉丝","2分钟"),
            ("水煮肉片","3分钟"),
            ("京酱肉丝","1分钟"),
            ("糖醋里脊","3分钟"),
            ("梅菜扣肉","5分钟"),
            ("蚂蚁上树","1分钟"),
            ("青椒肉丝","2分钟"),
            ("蒜泥白肉","4分钟"),
            ("香菇炒肉","1分钟"),
            ("辣椒炒肉","3分钟"),
            ("四喜丸子","3分钟"),
            ("红烧狮子头","9分钟"),
            ("农家小炒肉","2分钟"),
            ("红烧猪蹄","2分钟")]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return SimpleTableCell.height
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datas.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:SimpleTableCell? = tableView.dequeueReusableCellWithIdentifier("SimpleTableCell") as? SimpleTableCell//, forIndexPath: indexPath) as? SimpleTableCell
        if cell == nil{
            var nib = NSBundle.mainBundle().loadNibNamed("SimpleTableCell", owner: self, options: nil)
            cell = nib[0] as? SimpleTableCell
        }
        let (name,time) = datas[indexPath.row]
        cell?.lblName?.text = name
        cell?.lblTime.text = time
        cell?.thumbnailImageView.image = UIImage(named: "cellheader.jpg")
        return cell!
    }


}

