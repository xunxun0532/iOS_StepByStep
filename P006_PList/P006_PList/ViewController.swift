//
//  ViewController.swift
//  P006_PList
//
//  Created by yao_yu on 14-9-22.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView:UITableView!
    var recipeNames = [String]()
    var thumbnail = [String]()
    var prepTime = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        loadMyData()
    }
    
    func loadMyData(){
        var recipes = NSDictionary(contentsOfFile: NSBundle.mainBundle().pathForResource("recipes", ofType: ".plist")!)
        recipeNames = recipes["RecipeName"] as [String]
        thumbnail = recipes["Thumbnail"] as [String]
        prepTime = recipes["PrepTime"] as [String]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recipeNames.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return SimpleTableCell.height
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("SimpleTableCell") as? SimpleTableCell
        if cell == nil{
            var nib = NSBundle.mainBundle().loadNibNamed("SimpleTableCell", owner: self, options: nil)
            cell = nib[0] as? SimpleTableCell
        }
        
        var row = indexPath.row
        cell?.lblName.text = recipeNames[row]
        cell?.lblTime.text = prepTime[row]
        cell?.thumbnailImageView.image = UIImage(named: thumbnail[row])
        
        return cell!
    }
    
}

