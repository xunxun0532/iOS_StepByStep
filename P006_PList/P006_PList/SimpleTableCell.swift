//
//  SimpleTableCell.swift
//  P006_PList
//
//  Created by yao_yu on 14-9-22.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import UIKit

class SimpleTableCell: UITableViewCell {

    @IBOutlet weak var thumbnailImageView:UIImageView!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblTime:UILabel!
    
    class var height:CGFloat{
        return 78
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
