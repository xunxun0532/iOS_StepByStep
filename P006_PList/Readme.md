##使用属性表PropertyList文件
加载Plist文件, NSDictionary(contentsOfFile:)

    func loadMyData(){
        var recipes = NSDictionary(contentsOfFile: NSBundle.mainBundle().pathForResource("recipes", ofType: ".plist")!)
        recipeNames = recipes["RecipeName"] as [String]
        thumbnail = recipes["Thumbnail"] as [String]
        prepTime = recipes["PrepTime"] as [String]
    }

效果:

![预览](P006_PList/preview.png "主界面")