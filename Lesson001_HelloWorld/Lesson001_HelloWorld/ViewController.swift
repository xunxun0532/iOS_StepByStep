//
//  ViewController.swift
//  Lesson001_HelloWorld
//
//  Created by yao_yu on 14-9-22.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBAction func sayHello(sender: AnyObject) {
        var alert = UIAlertView(title: "Swift", message: "欢迎来到iOS世界", delegate: self, cancelButtonTitle: "确定")
        alert.show()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

